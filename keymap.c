#include QMK_KEYBOARD_H

enum layers {
    COLEMAK = 0,
    NAV,
    COLEMAK_SYM,
    QWERTY,
    LOWER,

    _GAME_LAYER
};

// Alpha Modifiers
#define MLC_A LCTL_T(KC_A)
#define MLA_R LSFT_T(KC_R)
#define MLS_S LGUI_T(KC_S)
#define MLG_T LALT_T(KC_T)

#define MRG_H RALT_T(KC_H)
#define MRS_E RGUI_T(KC_E)
#define MRA_I RSFT_T(KC_I)
#define MRC_O RCTL_T(KC_O)

// Arrow Modifiers
#define MLC_L LCTL_T(KC_LEFT)
#define MLA_U LSFT_T(KC_UP)
#define MLS_D LGUI_T(KC_DOWN)
#define MLG_R LALT_T(KC_RIGHT)

#define MRC_C RCTL_T(KC_SCLN)
#define MRA_R RSFT_T(KC_RIGHT)
#define MRS_D RGUI_T(KC_DOWN)
#define MRG_U RALT_T(KC_UP)


#define LS_LBRC LSFT_T(KC_LBRC)
#define RS_RBRC RSFT_T(KC_RBRC)
#define RS_MINS RSFT_T(KC_MINS)

#define RC_BSLS RCTL_T(KC_BSLS)

#define NAV_SPC LT(NAV, KC_SPC)
#define SYM_ENT LT(COLEMAK_SYM, KC_ENT)

#define ESC_CTL LCTL_T(KC_ESC)
#define ESC_GUI LGUI_T(KC_ESC)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
	[COLEMAK] = LAYOUT_ortho_5x15(
         KC_GRV,   KC_F1,    KC_2,    KC_3,    KC_4,    KC_5, KC_MPRV,    KC_MPLY, KC_MNXT,    KC_6,    KC_7,    KC_8,    KC_9,  KC_F11,  KC_F12,
         KC_TAB,    KC_1,    KC_W,    KC_F,    KC_P,    KC_B, KC_VOLD,    KC_MUTE, KC_VOLU,    KC_J,    KC_Y,    KC_U,    KC_K,    KC_0,  KC_EQL,
        ESC_GUI,    KC_Q,   MLA_R,   MLS_S,   MLG_T,    KC_G, KC_LBRC, DF(QWERTY), KC_RBRC,    KC_L,   MRG_H,   MRS_E,   MRA_I, KC_SCLN, KC_QUOT,
        KC_LSFT,   MLC_A,    KC_X,    KC_C,    KC_D,    KC_V,   KC_NO,      KC_NO,   KC_NO,    KC_M,    KC_N, KC_COMM,  KC_DOT,   MRC_O, RS_MINS,
        KC_LCTL,    KC_Z, TT(NAV), KC_LALT,  KC_DEL, NAV_SPC, SYM_ENT,      KC_NO, SYM_ENT, NAV_SPC, KC_BSPC, TT(NAV), KC_LBRC, KC_SLSH, RC_BSLS
    ),
	[NAV] = LAYOUT_ortho_5x15(
         KC_F12,   KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5, _______, _______, _______,   KC_F6,   KC_F7,   KC_F8,   KC_F9,  KC_F10,  KC_F11,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        KC_CAPS, _______,   MLA_U,   MLS_D,   MLG_R, _______, _______, _______, _______, KC_LEFT,   MRG_U,   MRS_D,   MRA_R, _______, _______,
        _______,   MLC_L, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,   MRC_C, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,   RESET
    ),
	[COLEMAK_SYM] = LAYOUT_ortho_5x15(
         KC_F12,   KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5, _______, _______, _______,   KC_F6,   KC_F7,   KC_F8,   KC_F9,  KC_F10,  KC_F11,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        KC_CAPS, _______, KC_LCBR, KC_LPRN, KC_QUOT, _______, _______, _______, _______, _______, KC_BSLS, KC_RPRN, KC_RCBR, _______, _______,
        _______, KC_LBRC, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, KC_RBRC, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,   RESET
    ),

/*
	[QWERTY] = LAYOUT_ortho_5x15(
        KC_VOLU, KC_MNXT,  KC_DEL,  KC_GRV,    KC_1,            KC_2,    KC_3,   KC_4,  KC_5,                   KC_6,     KC_7,    KC_8,    KC_9,    KC_0,         KC_BSPC,
        KC_MUTE, KC_MPLY, KC_HOME,  KC_TAB,    KC_Q,            KC_W,    KC_E,   KC_R,  KC_T,                   KC_Y,     KC_U,    KC_I,    KC_O,    KC_P,          KC_EQL,
        KC_VOLD, KC_MPRV,  KC_END,  KC_ESC,    KC_A,            KC_S,    KC_D,   KC_F,  KC_G,                   KC_H,     KC_J,    KC_K,    KC_L, KC_SCLN,         KC_QUOT,
          KC_UP, KC_DOWN, KC_PGUP, KC_LSFT,    KC_Z,            KC_X,    KC_C,   KC_V,  KC_B,                   KC_N,     KC_M, KC_COMM,  KC_DOT, KC_SLSH, RSFT_T(KC_MINS),
        KC_LEFT, KC_RGHT, KC_PGDN, KC_LCTL, KC_LGUI, MO(NAV), KC_LALT, KC_SPC, KC_NO, LT(NAV,KC_SPC),   KC_ENT, KC_BSPC, KC_LBRC, KC_RBRC, RCTL_T(KC_BSLS)
    ),
	[NAV] = LAYOUT_ortho_5x15(
            KC_NO, RGB_TOG, RGB_SPI,   KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,  KC_F10,  KC_F11,  KC_F12,
         RGB_RMOD, RGB_MOD, RGB_SPD, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_HOME, KC_PGUP, KC_PGDN,  KC_END, KC_TRNS, KC_TRNS,
          RGB_HUD, RGB_HUI, RGB_VAI, KC_LEFT,   KC_UP, KC_DOWN, KC_RGHT, KC_TRNS, KC_TRNS, KC_LEFT,   KC_UP, KC_DOWN, KC_RGHT, KC_TRNS,   RESET,
          RGB_SAD, RGB_SAI, RGB_VAD, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
          RGB_VAD, RGB_VAI,  KC_DEL, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS
    ),
    */
    // Split QWERTY
	[QWERTY] = LAYOUT_ortho_5x15(
         KC_GRV,    KC_1,    KC_2,    KC_3,   KC_4,   KC_5,    KC_6,     KC_MPLY, KC_MNXT,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,          KC_F12,
         KC_TAB,    KC_Q,    KC_W,    KC_E,   KC_R,   KC_T, KC_VOLD,     KC_MUTE, KC_VOLU,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,          KC_EQL,
         KC_ESC,    KC_A,    KC_S,    KC_D,   KC_F,   KC_G, KC_LBRC, DF(COLEMAK), KC_RBRC,    KC_H,    KC_J,    KC_K,    KC_L, KC_SCLN,         KC_QUOT,
        KC_LSFT,    KC_Z,    KC_X,    KC_C,   KC_V,   KC_B,    KC_N,       KC_UP, KC_RGHT,    KC_N,    KC_M, KC_COMM,  KC_DOT, KC_SLSH, RSFT_T(KC_MINS),
        KC_LCTL, KC_LGUI, TT(NAV), KC_LALT, KC_DEL, KC_SPC,  KC_ENT,     KC_DOWN,  KC_ENT, NAV_SPC, KC_BSPC, TT(NAV), KC_RGUI, KC_RALT, RCTL_T(KC_BSLS)
    ),
};

bool vim_hack = false;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch(keycode & 0xFF) {
        case KC_SCLN:
            if (vim_hack) {
                if(record->event.pressed) {
                    tap_code(KC_ESC);
                    _delay_ms(100);
                    register_code(KC_LSFT);
                    register_code(KC_SCLN);
                } else {
                    unregister_code(KC_SCLN);
                    unregister_code(KC_LSFT);
                }
                return false;
            }
            return true;
    }
    return true;
}

layer_state_t layer_state_set_user(layer_state_t state) {
    switch (get_highest_layer(state)) {
        case NAV:
            vim_hack = true;
            break;
        default:
            vim_hack = false;
            break;
    }

    return state;
}

/*
layer_state_t layer_state_set_user(layer_state_t state) {
  switch (get_highest_layer(state)) {

        case _NAV_LAYER:
            rgblight_enable();
            rgblight_setrgb (RGB_YELLOW);
            break;
        case _PRI_LAYER:
        default:
            rgblight_disable();
            break;
    }

    return state;
}
*/
